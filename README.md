# 在线考试
[演示地址](http://47.92.221.134:8080)

## 组卷
![输入图片说明](https://images.gitee.com/uploads/images/2021/0806/144840_bb72dd0d_393390.png "屏幕截图.png")

## 编辑试题
![输入图片说明](https://images.gitee.com/uploads/images/2021/0806/143937_09a65f42_393390.png "屏幕截图.png")
## 导入试题
![输入图片说明](https://images.gitee.com/uploads/images/2021/0806/144955_68fb934a_393390.png "屏幕截图.png")

## 账号
系统管理员	admin	111111

## 技术实现
bs架构，采用开源组件jdk8、mysql5.7、springboot2x、elementUI2x

## qq群
811189776

## 开源项目推荐
### WCP:知识管理系统（内部资料上传检索知识共享，建议考试系统搭配） https://gitee.com/macplus/WCP
### WDA:文件转换组件（附件在线预览）https://gitee.com/macplus/WDA
### WTS:在线答题系统 https://gitee.com/macplus/WTS
### WLP:在线学习系统 https://gitee.com/macplus/WLP
### PLOGS:项目任务日志管理系统（项目进度跟踪） https://gitee.com/macplus/plogs